package edu.hope.cs.csci392.imdb.services;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import edu.hope.cs.csci392.imdb.Database;
import edu.hope.cs.csci392.imdb.model.Actor;
import edu.hope.cs.csci392.imdb.model.Role;

@Path("actors")
@Produces(MediaType.APPLICATION_JSON)
public class ActorService {

	private static final long serialVersionUID = -4129386223171713871L;

	private static class SearchRequest {
		public String stageFirstName;
		public String stageLastName;
		public String birthCity;
		public String birthStateOrProvince;
	}
		
	@GET
	@Path("{id}")
	public Response findActorByID(@PathParam("id") int actorID) {
		Database db = Database.getInstance();
		Actor actor = db.findActorByID(actorID);
		return Response.ok(actor).build();
	}
	
	@GET
	@Path("{id}/roles")
	public Response findRolesForActor(@PathParam("id") int actorID) {
		Database db = Database.getInstance();
		List<Role> roles = db.findMoviesForActor(db.findActorByID(actorID));
		return Response.ok(roles).build();
	}
	
	@POST
	@Path("search")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response searchByActor(SearchRequest request) {							
		Database db = Database.getInstance();
		List<Actor> actors = db.findActors(request.stageFirstName, request.stageLastName, request.birthCity, request.birthStateOrProvince);
		return Response.ok(actors).build();		
	}
	
}
